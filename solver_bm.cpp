#include "ml_config.h"

#include "mpi.h"
#include "Epetra_MpiComm.h"
#include "Epetra_Map.h"
#include "Epetra_Vector.h"
#include "Epetra_Time.h"
#include "Epetra_RowMatrix.h"
#include "Epetra_CrsMatrix.h"
#include "Epetra_VbrMatrix.h"
#include "Teuchos_ParameterList.hpp"
#include "ml_MultiLevelPreconditioner.h"
#include "AztecOO.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <boost/scope_exit.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/range/numeric.hpp>

#include "domain_partition.hpp"

using namespace Teuchos;

boost::shared_ptr< domain_partition<3> > part;

std::vector<int> renum, inv_renum;

//---------------------------------------------------------------------------
void summary(
        const char *desc, double etm, int itr,
        Epetra_CrsMatrix const &A,
        Epetra_Vector    const &f,
        Epetra_Vector          &x
        )
{
    double Norm;
    Epetra_Vector Ax( f.Map() );
    A.Multiply(false, x, Ax);
    Ax.Update(1.0, f, -1.0);
    Ax.Norm2(&Norm);

    if (A.Comm().MyPID() == 0) {
        std::cout << desc << "......Using " << A.Comm().NumProc() << " processes" << std::endl;
        std::cout << desc << "......||A x - b||_2 = " << Norm << std::endl;
        std::cout << desc << "......Total Time = " << etm << std::endl;
        std::cout << desc << "......Iterations = " << itr << std::endl;
    }
}

//---------------------------------------------------------------------------
int partition(const Epetra_MpiComm &Comm, int n)
{
    int n3 = n * n * n;

    boost::array<int,3> lo = { { 0,   0,   0 } };
    boost::array<int,3> hi = { {n-1, n-1, n-1} };

    part = boost::make_shared< domain_partition<3> >(lo, hi, Comm.NumProc());
    int chunk = part->size( Comm.MyPID() );

    std::vector<int> domain(Comm.NumProc() + 1, 0);
    Comm.GatherAll(&chunk, &domain[1], 1);
    boost::partial_sum(domain, domain.begin());

    renum.resize(n3);
    inv_renum.resize(n3);
    for(int k = 0, idx = 0; k < n; ++k) {
        for(int j = 0; j < n; ++j) {
            for(int i = 0; i < n; ++i, ++idx) {
                boost::array<int, 3> p = {{i, j, k}};
                std::pair<int,int> v = part->index(p);
                renum[idx] = domain[v.first] + v.second;
                inv_renum[renum[idx]] = idx;
            }
        }
    }

    return chunk;
}

//---------------------------------------------------------------------------
boost::shared_ptr<Epetra_CrsMatrix> Laplace3D(
        const Epetra_Map &Map, int n, Epetra_Vector &rhs
        )
{
    boost::shared_ptr<Epetra_CrsMatrix> A = boost::make_shared<Epetra_CrsMatrix>(
            Copy, Map, 7);

    std::vector<int>    col(7);
    std::vector<double> val(7);

    double h = 1.0 / (n - 1);
    double h2inv = (n - 1) * (n - 1);

    int *gid = Map.MyGlobalElements();
    for(int row = 0; row < Map.NumMyElements(); ++row) {
        int idx = gid[row];
        int k = inv_renum[idx];
        int i = k % n; k /= n;
        int j = k % n; k /= n;

        col.clear();
        val.clear();

        if (k > 0)  {
            col.push_back(renum[inv_renum[idx] - n * n]);
            val.push_back(-h2inv);
        }

        if (j > 0)  {
            col.push_back(renum[inv_renum[idx] - n]);
            val.push_back(-h2inv);
        }

        if (i > 0) {
            col.push_back(renum[inv_renum[idx] - 1]);
            val.push_back(-h2inv);
        }

        col.push_back(idx);
        val.push_back(6 * h2inv);

        if (i + 1 < n) {
            col.push_back(renum[inv_renum[idx] + 1]);
            val.push_back(-h2inv);
        }

        if (j + 1 < n) {
            col.push_back(renum[inv_renum[idx] + n]);
            val.push_back(-h2inv);
        }

        if (k + 1 < n) {
            col.push_back(renum[inv_renum[idx] + n * n]);
            val.push_back(-h2inv);
        }

        rhs[row] = 1.0;

        A->InsertGlobalValues(idx, col.size(), val.data(), col.data());
    }
    A->FillComplete();

    return A;
}

//---------------------------------------------------------------------------
// Trilinos solver
//---------------------------------------------------------------------------
void TestTrilinos(
        Epetra_CrsMatrix &A,
        Epetra_Vector    &f,
        Epetra_Vector    &x
        )
{
    const Epetra_Comm &Comm = A.Comm();

    Epetra_Time Time(Comm);

    // =================== //
    // call ML and AztecOO //
    // =================== //

    Epetra_LinearProblem Problem(&A, &x, &f);
    AztecOO solver(Problem);

    ParameterList MLList;

    ML_Epetra::SetDefaults("NSSA", MLList);
    ML_Epetra::MultiLevelPreconditioner MLPrec(A, MLList, true);

    // tell AztecOO to use this preconditioner, then solve
    solver.SetPrecOperator(&MLPrec);

    solver.SetAztecOption(AZ_solver, AZ_cg);
    solver.SetAztecOption(AZ_output, 32);

    solver.Iterate(1000, 1e-8);

    double etm = Time.ElapsedTime();

    summary("Trilinos", etm, solver.NumIters(), A, f, x);
}

//---------------------------------------------------------------------------
void run_tests(const char *desc, Epetra_CrsMatrix &A, Epetra_Vector &RHS, int nexp)
{
    Epetra_Vector X(RHS.Map());

    for(int m = 0; m < nexp; ++m) {
        X.PutScalar(0.0);
        TestTrilinos(A, RHS, X);
    }
}

//---------------------------------------------------------------------------
int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);
    Epetra_MpiComm Comm(MPI_COMM_WORLD);
    BOOST_SCOPE_EXIT(void) {
        MPI_Finalize();
    } BOOST_SCOPE_EXIT_END

    int n = argc < 2 ? 32 : atoi(argv[1]);
    int m = argc < 3 ?  5 : atoi(argv[2]);
    int n3 = n * n * n;

    // Partition the domain
    partition(Comm, n);

    int chunk = part->size( Comm.MyPID() );

    Epetra_Map Map(n3, part->size(Comm.MyPID()), 0, Comm);
    Epetra_Vector RHS(Map);
    boost::shared_ptr<Epetra_CrsMatrix> A;

    A = Laplace3D(Map, n, RHS);
    run_tests("laplace3d", *A, RHS, m);
}
